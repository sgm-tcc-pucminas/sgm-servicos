#CRIAR
kubectl create -f postgres-configmap.yaml -n gitlab-managed-apps
kubectl create -f postgres-storage.yaml -n gitlab-managed-apps
kubectl create -f postgres-deployment.yaml -n gitlab-managed-apps
kubectl create -f postgres-service.yaml -n gitlab-managed-apps


# REMOVER
kubectl delete service postgres -n gitlab-managed-apps
kubectl delete deployment postgres -n gitlab-managed-apps
kubectl delete configmap postgres-config -n gitlab-managed-apps
kubectl delete persistentvolumeclaim postgres-pv-claim -n gitlab-managed-apps
kubectl delete persistentvolume postgres-pv-volume -n gitlab-managed-apps