--SERVICO
drop table if exists servicosolicitacao;
drop table if exists statussolicitacao;
drop table if exists servico;

drop table if exists vagasolicitacao;
drop table if exists vaga;

CREATE TABLE servico (
	id bigserial NOT NULL,
	descricao varchar NOT null,
	valor numeric(11,2) NOT NULL,
	prazoatendimento int NOT NULL,
	unidademedidaprazo varchar(10) NOT NULL,
	CONSTRAINT servico_id_pk PRIMARY KEY (id)
);

CREATE TABLE statussolicitacao (
	id bigserial NOT NULL,
	descricao varchar NOT null,
	CONSTRAINT statussolicitacao_id_pk PRIMARY KEY (id)
);

CREATE TABLE statusexecucaoservico (
	id bigserial NOT NULL,
	descricao varchar NOT null,
	CONSTRAINT statusexecucaoservico_id_pk PRIMARY KEY (id)
);

CREATE TABLE servicosolicitacao (
	id bigserial NOT NULL,
	idservico bigint not null,
	idusuariosolicitacao bigint not null,
	cep integer not null,
	endereco varchar,
	observacao varchar,
	idstatussolicitacao bigint NOT null,
	responsavel varchar(255),
	contato varchar(100),
	datahorasolicitacao timestamp not null,
	CONSTRAINT servicosolicitacao_id_pk PRIMARY KEY (id),
	CONSTRAINT servicosolicitacao_idservico_fk FOREIGN KEY (idservico)
          REFERENCES servico (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action,
    CONSTRAINT servicosolicitacao_idstatussolicitacao_fk FOREIGN KEY (idstatussolicitacao)
            REFERENCES statussolicitacao (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action
);

CREATE TABLE servicoexecucao (
	id bigserial NOT NULL,
	idservicosolicitacao bigint not null,
	idresponsavel bigint not null,
	idusuarioaprovacao bigint not null,
	idstatusexecucao bigint NOT null,
	datahoraexecucao timestamp,
	CONSTRAINT servicoexecucao_id_pk PRIMARY KEY (id),
	CONSTRAINT servicoexecucao_idservicosolicitacao_fk FOREIGN KEY (idservicosolicitacao)
          REFERENCES servicosolicitacao (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action,
    CONSTRAINT servicoexecucao_idstatusexecucao_fk FOREIGN KEY (idstatusexecucao)
            REFERENCES statusexecucaoservico (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action
);

insert into statussolicitacao values (1, 'PENDENTE');
insert into statussolicitacao values (2, 'PROCESSANDO');
insert into statussolicitacao values (3, 'FINALIZADA');

insert into statusexecucaoservico values (1, 'PENDENTE');
insert into statusexecucaoservico values (2, 'PROCESSANDO');
insert into statusexecucaoservico values (3, 'FINALIZADA');

CREATE TABLE vaga (
	id bigserial NOT NULL,
	idEscola bigint not null,
	nome varchar not null,
	logradouro varchar not null,
	bairro varchar NOT null,
	cep int not null,
	CONSTRAINT vaga_id_pk PRIMARY KEY (id)
);

CREATE TABLE vagasolicitacao (
	id bigserial NOT NULL,
	idVaga bigint not null,
	idUsuarioSolicitacao bigint not null,
	idstatussolicitacao bigint not null,
	datahorasolicitacao timestamp not null,
	CONSTRAINT vagasolicitacao_id_pk PRIMARY KEY (id),
	CONSTRAINT servicosolicitacao_idstatussolicitacao_fk FOREIGN KEY (idstatussolicitacao)
                REFERENCES statussolicitacao (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action
);

CREATE TABLE vagaaprovacao (
	id bigserial NOT NULL,
	idvagasolicitacao bigint not null,
	idusuarioaprovacao bigint not null,
	datahoraaprovacao timestamp not null,
	CONSTRAINT vagaaprovacao_id_pk PRIMARY KEY (id),
	CONSTRAINT vagaaprovacao_idvagasolicitacao_fk FOREIGN KEY (idvagasolicitacao)
          REFERENCES vagasolicitacao (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action
);

--GEORREFERENCIA
drop table if exists endereco;
drop table if exists bairro;

CREATE TABLE bairro (
	id bigserial NOT NULL,
	nome varchar NOT null,
	CONSTRAINT bairro_id_pk PRIMARY KEY (id)
);

CREATE TABLE endereco (
	cep int NOT NULL,
	prefixo varchar NOT null,
	logradouro varchar NOT null,
	idbairroinicio bigint NOT null,
	idbairrofim bigint,
	imagemurl varchar,
	CONSTRAINT endereco_cep_pk PRIMARY KEY (cep),
    CONSTRAINT endereco_idbairroinicio_fk FOREIGN KEY (idbairroinicio)
          REFERENCES bairro (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action,
    CONSTRAINT endereco_idbairrofim_fk FOREIGN KEY (idbairrofim)
          REFERENCES bairro (id) MATCH simple ON UPDATE NO ACTION ON DELETE NO action
);