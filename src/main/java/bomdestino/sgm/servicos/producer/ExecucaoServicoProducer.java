package bomdestino.sgm.servicos.producer;

import bomdestino.sgm.servicos.model.message.ExecucaoServicoMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExecucaoServicoProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(ExecucaoServicoMessage message) {
        rabbitTemplate.convertAndSend("servico-solicitacao-execucao", message);
    }
}
