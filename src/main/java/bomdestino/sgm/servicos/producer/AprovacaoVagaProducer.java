package bomdestino.sgm.servicos.producer;

import bomdestino.sgm.servicos.model.message.AprovacaoVagaMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AprovacaoVagaProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(AprovacaoVagaMessage message) {
        rabbitTemplate.convertAndSend("educacao-solicitacao-vaga", message);
    }
}
