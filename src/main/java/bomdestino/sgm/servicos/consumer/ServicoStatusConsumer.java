package bomdestino.sgm.servicos.consumer;

import bomdestino.sgm.servicos.model.message.ServicoStatusMessage;
import bomdestino.sgm.servicos.service.ServicoSolicitacaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServicoStatusConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServicoSolicitacaoService service;

    @RabbitListener(queues = "servico-atualizacao-status")
    public void executar(ServicoStatusMessage message) {
        logger.info("Obtendo mensagem para obter status de solicitacao");
        service.atualizarStatusSolicitacao(message.getIdSolicitacaoServico(), message.getDataHoraExecucao());
    }
}
