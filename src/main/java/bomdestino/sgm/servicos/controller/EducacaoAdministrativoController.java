package bomdestino.sgm.servicos.controller;

import bomdestino.sgm.servicos.config.security.User;
import bomdestino.sgm.servicos.model.dto.request.VagaRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.EscolaResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaSolicitacaoListaResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaSolicitacaoResponseDTO;
import bomdestino.sgm.servicos.service.EscolaService;
import bomdestino.sgm.servicos.service.VagaService;
import bomdestino.sgm.servicos.service.VagaSolicitacaoService;
import bomdestino.sgm.servicos.util.token.TokenUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/administrative/")
public class EducacaoAdministrativoController {

    @Autowired
    private VagaService vagaService;

    @Autowired
    private VagaSolicitacaoService solicitacaoService;

    @Autowired
    private EscolaService escolaService;

    @Autowired
    private TokenUtil tokenUtil;

    @ApiOperation(
            value = "Cadastrar nova vaga",
            notes = "Cadastro de nova vaga na base de dados",
            response = VagaResponseDTO.class
    )
    @PostMapping("/v1/educacao/vaga")
    public ResponseEntity<VagaResponseDTO> criar(@RequestBody VagaRequestDTO request) {
        VagaResponseDTO vaga = vagaService.criar(request);
        return ResponseEntity.ok().body(vaga);
    }

    @ApiOperation(
            value = "Cadastrar nova vaga",
            notes = "Cadastro de nova vaga na base de dados",
            response = VagaResponseDTO.class
    )
    @GetMapping("/v1/educacao/escolas")
    public ResponseEntity<EscolaResponseDTO> obterEscolas() {
        EscolaResponseDTO escolas = escolaService.obter();

        if (isNull(escolas) || escolas.getEscolas().isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(escolas);
        }
    }

    @ApiOperation(
            value = "Obter Solicitação de Vaga",
            notes = "Obtem as solicitações de vaga com base no status informado na requisição",
            response = VagaSolicitacaoListaResponseDTO.class
    )
    @GetMapping("/v1/educacao/vaga/solicitacao/{status}")
    public ResponseEntity<VagaSolicitacaoListaResponseDTO> obter(@PathVariable("status") Integer status) {
        List<VagaSolicitacaoResponseDTO> solicitacoes = solicitacaoService.obter(status);

        if (solicitacoes.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new VagaSolicitacaoListaResponseDTO(solicitacoes));
        }
    }

    @ApiOperation(
            value = "Aprovar Solicitação de Vaga",
            notes = "Aprovar a solicitação de vaga com base no id informado na requisição",
            response = VagaSolicitacaoResponseDTO.class
    )
    @PutMapping("/v1/educacao/vaga/solicitacao/{idSolicitacao}")
    public ResponseEntity<VagaSolicitacaoResponseDTO> aprovar(@PathVariable("idSolicitacao") Long idSolicitacao) {
        User usuarioLogado = tokenUtil.getAuthUser();
        solicitacaoService.aprovarSolicitacao(idSolicitacao, usuarioLogado.getId());
        return ResponseEntity.noContent().build();
    }
}