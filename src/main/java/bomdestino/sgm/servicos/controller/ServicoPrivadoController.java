package bomdestino.sgm.servicos.controller;

import bomdestino.sgm.servicos.config.security.User;
import bomdestino.sgm.servicos.model.dto.ServicoSolicitacaoDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoSolicitacaoRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoSolicitacaoListaResponseDTO;
import bomdestino.sgm.servicos.service.ServicoService;
import bomdestino.sgm.servicos.service.ServicoSolicitacaoService;
import bomdestino.sgm.servicos.util.token.TokenUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/private")
public class ServicoPrivadoController {

    @Autowired
    private ServicoService service;

    @Autowired
    private ServicoSolicitacaoService solicitacaoService;

    @Autowired
    private TokenUtil tokenUtil;

    @ApiOperation(
            value = "Solicitar Serviço",
            notes = "Solicitar a execução de um novo serviço",
            response = ServicoSolicitacaoDTO.class
    )
    @PostMapping("/v1/servico/solicitacao")
    public ResponseEntity<ServicoSolicitacaoDTO> criar(@RequestBody ServicoSolicitacaoRequestDTO solicitacao) {
        User usuarioLogado = tokenUtil.getAuthUser();
        ServicoSolicitacaoDTO servicoSolicitacao = solicitacaoService.salvar(solicitacao, usuarioLogado.getId());

        if (isNull(servicoSolicitacao)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(servicoSolicitacao);
        }
    }

    @ApiOperation(
            value = "Obter Serviços Solicitados",
            notes = "Obtem lista com todos os serviços solicitados pelo usuário logado",
            response = ServicoSolicitacaoListaResponseDTO.class
    )
    @GetMapping("/v1/servico/solicitacao")
    public ResponseEntity<ServicoSolicitacaoListaResponseDTO> obter() {
        User usuarioLogado = tokenUtil.getAuthUser();
        List<ServicoSolicitacaoDTO> solicitacoes = solicitacaoService.obterPorUsuario(usuarioLogado.getId());

        if (solicitacoes.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new ServicoSolicitacaoListaResponseDTO(solicitacoes));
        }
    }

    @ApiOperation(
            value = "Obter Serviço Solicitado por Protocolo",
            notes = "Obtem o serviço solicitado com base no número de protocolo informado",
            response = ServicoSolicitacaoDTO.class
    )
    @GetMapping("/v1/servico/solicitacao/{id}")
    public ResponseEntity<ServicoSolicitacaoDTO> obter(@PathVariable("id") Long idSolicitacao) {
        User usuarioLogado = tokenUtil.getAuthUser();
        ServicoSolicitacaoDTO servico = solicitacaoService.obterPorId(idSolicitacao, usuarioLogado.getId());

        if (isNull(servico)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(servico);
        }
    }
}
