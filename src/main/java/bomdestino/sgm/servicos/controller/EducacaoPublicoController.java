package bomdestino.sgm.servicos.controller;

import bomdestino.sgm.servicos.model.dto.response.VagaListaResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaResponseDTO;
import bomdestino.sgm.servicos.service.VagaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/public")
public class EducacaoPublicoController {

    @Autowired
    private VagaService vagaService;

    @ApiOperation(
            value = "Obter Todas Vagas",
            notes = "Obtêm todas as vagas disponíveis",
            response = VagaListaResponseDTO.class
    )
    @GetMapping("/v1/educacao/vaga")
    public ResponseEntity<VagaListaResponseDTO> obterVagas() {
        List<VagaResponseDTO> vagas = vagaService.obterTodas();

        if (isNull(vagas)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new VagaListaResponseDTO(vagas));
        }
    }
}