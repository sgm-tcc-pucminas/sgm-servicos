package bomdestino.sgm.servicos.controller;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoPublicoResponseDTO;
import bomdestino.sgm.servicos.service.ServicoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/public")
public class ServicoPublicoController {

    @Autowired
    private ServicoService service;

    @ApiOperation(
            value = "Obter Serviços Disponiveis",
            notes = "Obtem lista com todos os serviços disponibilizados",
            response = ServicoPublicoResponseDTO.class
    )
    @GetMapping("/v1/servico")
    public ResponseEntity<ServicoPublicoResponseDTO> obter() {
        List<ServicoDTO> servicos = service.obterTodos();

        if (servicos.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new ServicoPublicoResponseDTO(servicos));
        }
    }
}