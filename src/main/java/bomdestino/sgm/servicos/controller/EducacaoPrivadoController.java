package bomdestino.sgm.servicos.controller;

import bomdestino.sgm.servicos.config.security.User;
import bomdestino.sgm.servicos.model.dto.request.VagaSolicitacaoRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaSolicitacaoListaResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaSolicitacaoResponseDTO;
import bomdestino.sgm.servicos.service.VagaSolicitacaoService;
import bomdestino.sgm.servicos.util.token.TokenUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/private")
public class EducacaoPrivadoController {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private VagaSolicitacaoService vagaSolicitacaoService;

    @ApiOperation(
            value = "Solicitar Vaga",
            notes = "Cadastrar uma solicitação de vaga",
            response = VagaSolicitacaoResponseDTO.class
    )
    @PostMapping("/v1/educacao/vaga")
    public ResponseEntity<VagaSolicitacaoResponseDTO> criar(@RequestBody VagaSolicitacaoRequestDTO vaga) {
        User usuarioLogado = tokenUtil.getAuthUser();
        VagaSolicitacaoResponseDTO vagaSolicitacao = vagaSolicitacaoService.criar(vaga, usuarioLogado.getId());
        return ResponseEntity.ok().body(vagaSolicitacao);
    }

    @ApiOperation(
            value = "Obter Todas Solicitaçao",
            notes = "Obtêm todas as solicitações do usuário logado",
            response = VagaSolicitacaoListaResponseDTO.class
    )
    @GetMapping("/v1/educacao/vaga")
    public ResponseEntity<VagaSolicitacaoListaResponseDTO> obter() {
        User usuarioLogado = tokenUtil.getAuthUser();
        List<VagaSolicitacaoResponseDTO> solicitacoes = vagaSolicitacaoService.obterPorUsuario(usuarioLogado.getId());

        if (solicitacoes.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new VagaSolicitacaoListaResponseDTO(solicitacoes));
        }
    }

    @ApiOperation(
            value = "Obter Solicitação de Vaga",
            notes = "Obter a solicitação de vaga com base no protocolo informado",
            response = VagaSolicitacaoResponseDTO.class
    )
    @GetMapping("/v1/educacao/vaga/{idSolicitacao}")
    public ResponseEntity<VagaSolicitacaoResponseDTO> obter(@PathVariable("idSolicitacao") Long idSolicitacao) {
        VagaSolicitacaoResponseDTO solicitacao = vagaSolicitacaoService.obterPorId(idSolicitacao, tokenUtil.getAuthUser());

        if (isNull(solicitacao)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(solicitacao);
        }
    }
}