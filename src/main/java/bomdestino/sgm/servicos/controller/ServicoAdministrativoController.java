package bomdestino.sgm.servicos.controller;

import bomdestino.sgm.servicos.config.security.User;
import bomdestino.sgm.servicos.model.dto.ServicoSolicitacaoDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoExecucaoRequestDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoResponsavelReponseDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoSolicitacaoListaResponseDTO;
import bomdestino.sgm.servicos.service.ServicoService;
import bomdestino.sgm.servicos.service.ServicoSolicitacaoService;
import bomdestino.sgm.servicos.util.token.TokenUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/administrative/")
public class ServicoAdministrativoController {

    @Autowired
    private ServicoService servicoService;

    @Autowired
    private ServicoSolicitacaoService solicitacaoService;

    @Autowired
    private TokenUtil tokenUtil;

    @ApiOperation(
            value = "Criar Novo Serviço",
            notes = "Cadastrar novo serviço na base de dados",
            response = ServicoResponseDTO.class
    )
    @PostMapping("/v1/servico")
    public ResponseEntity<ServicoResponseDTO> criarServico(@RequestBody ServicoRequestDTO servico) {
        ServicoResponseDTO response = servicoService.criar(servico);
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation(
            value = "Obter Responsável Serviço",
            notes = "Obter lista com os responsáveis pelo atendimento do serviço informado",
            response = ServicoResponsavelReponseDTO.class
    )
    @GetMapping("/v1/servico/responsavel/{idServico}")
    public ResponseEntity<ServicoResponsavelReponseDTO> obterServicoResponsavel(@PathVariable("idServico") Long idServico) {
        ServicoResponsavelReponseDTO responsaveis = servicoService.obterServicoResponsavel(idServico);

        if (isNull(responsaveis) || responsaveis.getResponsaveis().isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(responsaveis);
        }
    }

    @ApiOperation(
            value = "Obter Solicitações de Serviço",
            notes = "Obter todas as solicitações de serviço cadastradas",
            response = ServicoSolicitacaoListaResponseDTO.class
    )
    @GetMapping("/v1/servico/solicitacao/{idStatus}")
    public ResponseEntity<ServicoSolicitacaoListaResponseDTO> obter(@PathVariable("idStatus") Integer idStatus) {
        List<ServicoSolicitacaoDTO> solicitacoes = solicitacaoService.obterPorStatus(idStatus);

        if (solicitacoes.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new ServicoSolicitacaoListaResponseDTO(solicitacoes));
        }
    }

    @ApiOperation(
            value = "Aprovar Solicitação de Serviço",
            notes = "Aprovar a solicitação de serviço com base no id informado"
    )
    @PostMapping("/v1/servico/solicitacao")
    public ResponseEntity<Void> aprovar(@RequestBody ServicoExecucaoRequestDTO servico) {
        User usuarioLogado = tokenUtil.getAuthUser();
        solicitacaoService.aprovar(servico, usuarioLogado.getId());
        return ResponseEntity.noContent().build();
    }
}