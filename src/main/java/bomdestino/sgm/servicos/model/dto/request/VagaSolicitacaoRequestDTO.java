package bomdestino.sgm.servicos.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VagaSolicitacaoRequestDTO implements Serializable {

    @JsonProperty("idvaga")
    private Long idVaga;

    public Long getIdVaga() {
        return idVaga;
    }

    public VagaSolicitacaoRequestDTO setIdVaga(Long idVaga) {
        this.idVaga = idVaga;
        return this;
    }
}