package bomdestino.sgm.servicos.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoSolicitacaoDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("servico")
    private ServicoDTO servico;
    @JsonProperty("usuarioSolicitacao")
    private UsuarioDTO usuarioSolicitacao;
    @JsonProperty("cep")
    private Integer cep;
    @JsonProperty("endereco")
    private String endereco;
    @JsonProperty("observacao")
    private String observacao;
    @JsonProperty("status")
    private String status;
    @JsonProperty("responsavel")
    private String responsavel;
    @JsonProperty("contato")
    private String contato;
    @JsonProperty("dataHoraSolicitacao")
    private LocalDateTime dataHoraSolicitacao;

    public Long getId() {
        return id;
    }

    public ServicoSolicitacaoDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public ServicoDTO getServico() {
        return servico;
    }

    public ServicoSolicitacaoDTO setServico(ServicoDTO servico) {
        this.servico = servico;
        return this;
    }

    public UsuarioDTO getUsuarioSolicitacao() {
        return usuarioSolicitacao;
    }

    public ServicoSolicitacaoDTO setUsuarioSolicitacao(UsuarioDTO usuarioSolicitacao) {
        this.usuarioSolicitacao = usuarioSolicitacao;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public ServicoSolicitacaoDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getEndereco() {
        return endereco;
    }

    public ServicoSolicitacaoDTO setEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public String getObservacao() {
        return observacao;
    }

    public ServicoSolicitacaoDTO setObservacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public ServicoSolicitacaoDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public ServicoSolicitacaoDTO setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public ServicoSolicitacaoDTO setContato(String contato) {
        this.contato = contato;
        return this;
    }

    public LocalDateTime getDataHoraSolicitacao() {
        return dataHoraSolicitacao;
    }

    public ServicoSolicitacaoDTO setDataHoraSolicitacao(LocalDateTime dataHoraSolicitacao) {
        this.dataHoraSolicitacao = dataHoraSolicitacao;
        return this;
    }
}