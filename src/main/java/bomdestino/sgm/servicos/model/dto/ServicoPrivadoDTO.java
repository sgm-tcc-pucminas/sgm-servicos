package bomdestino.sgm.servicos.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoPrivadoDTO implements Serializable {

    @JsonProperty("protocolo")
    private String protocolo;
    @JsonProperty("status")
    private String status;
    @JsonProperty("datahorasolicitacao")
    private LocalDateTime dataHoraSolicitacao;

    public String getProtocolo() {
        return protocolo;
    }

    public ServicoPrivadoDTO setProtocolo(String protocolo) {
        this.protocolo = protocolo;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public ServicoPrivadoDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    public LocalDateTime getDataHoraSolicitacao() {
        return dataHoraSolicitacao;
    }

    public ServicoPrivadoDTO setDataHoraSolicitacao(LocalDateTime dataHoraSolicitacao) {
        this.dataHoraSolicitacao = dataHoraSolicitacao;
        return this;
    }
}