package bomdestino.sgm.servicos.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("descricao")
    private String descricao;
    @JsonProperty("valor")
    private BigDecimal valor;
    @JsonProperty("prazo")
    private Integer prazo;
    @JsonProperty("unidade_prazo")
    private String unidadePrazo;

    public Long getId() {
        return id;
    }

    public ServicoDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDescricao() {
        return descricao;
    }

    public ServicoDTO setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public ServicoDTO setValor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public ServicoDTO setPrazo(Integer prazo) {
        this.prazo = prazo;
        return this;
    }

    public String getUnidadePrazo() {
        return unidadePrazo;
    }

    public ServicoDTO setUnidadePrazo(String unidadePrazo) {
        this.unidadePrazo = unidadePrazo;
        return this;
    }
}