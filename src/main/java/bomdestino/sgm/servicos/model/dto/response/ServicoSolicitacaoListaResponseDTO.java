package bomdestino.sgm.servicos.model.dto.response;

import bomdestino.sgm.servicos.model.dto.ServicoSolicitacaoDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoSolicitacaoListaResponseDTO implements Serializable {

    public List<ServicoSolicitacaoDTO> servicoLista;

    public ServicoSolicitacaoListaResponseDTO() {
    }

    public ServicoSolicitacaoListaResponseDTO(List<ServicoSolicitacaoDTO> servicos) {
        this.servicoLista = servicos;
    }

    public List<ServicoSolicitacaoDTO> getServicoLista() {
        return servicoLista;
    }

    public ServicoSolicitacaoListaResponseDTO setServicoLista(List<ServicoSolicitacaoDTO> servicoLista) {
        this.servicoLista = servicoLista;
        return this;
    }
}