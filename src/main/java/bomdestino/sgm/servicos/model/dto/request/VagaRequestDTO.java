package bomdestino.sgm.servicos.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VagaRequestDTO implements Serializable {

    @JsonProperty("idescola")
    private Long idEscola;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("logradouro")
    private String logradouro;
    @JsonProperty("bairro")
    private String bairro;
    @JsonProperty("cep")
    private Integer cep;

    public Long getIdEscola() {
        return idEscola;
    }

    public VagaRequestDTO setIdEscola(Long idEscola) {
        this.idEscola = idEscola;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public VagaRequestDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public VagaRequestDTO setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public VagaRequestDTO setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public VagaRequestDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }
}