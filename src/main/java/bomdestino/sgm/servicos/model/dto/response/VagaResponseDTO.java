package bomdestino.sgm.servicos.model.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VagaResponseDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("idescola")
    private Long idEscola;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("logradouro")
    private String logradouro;
    @JsonProperty("bairro")
    private String bairro;
    @JsonProperty("cep")
    private Integer cep;
    @JsonProperty("contato")
    private String contato;
    @JsonProperty("responsavel")
    private String responsavel;

    public Long getId() {
        return id;
    }

    public VagaResponseDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getIdEscola() {
        return idEscola;
    }

    public VagaResponseDTO setIdEscola(Long idEscola) {
        this.idEscola = idEscola;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public VagaResponseDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public VagaResponseDTO setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public VagaResponseDTO setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public VagaResponseDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public VagaResponseDTO setContato(String contato) {
        this.contato = contato;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public VagaResponseDTO setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }
}