package bomdestino.sgm.servicos.model.dto.response;

import bomdestino.sgm.servicos.model.dto.ServicoSolicitacaoDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoSolicitacaoResponseDTO implements Serializable {

    private List<ServicoSolicitacaoDTO> servicoLista;

    public ServicoSolicitacaoResponseDTO() {
    }

    public ServicoSolicitacaoResponseDTO(List<ServicoSolicitacaoDTO> servicoLista) {
        this.servicoLista = servicoLista;
    }

    public List<ServicoSolicitacaoDTO> getServicoLista() {
        return servicoLista;
    }

    public ServicoSolicitacaoResponseDTO setServicoLista(List<ServicoSolicitacaoDTO> servicoLista) {
        this.servicoLista = servicoLista;
        return this;
    }
}