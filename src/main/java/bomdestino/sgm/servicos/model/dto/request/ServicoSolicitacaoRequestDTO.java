package bomdestino.sgm.servicos.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoSolicitacaoRequestDTO implements Serializable {

    @JsonProperty("idservico")
    private Long idServico;
    @JsonProperty("endereco")
    private String endereco;
    @JsonProperty("cep")
    private Integer cep;
    @JsonProperty("observacao")
    private String observacao;

    public Long getIdServico() {
        return idServico;
    }

    public ServicoSolicitacaoRequestDTO setIdServico(Long idServico) {
        this.idServico = idServico;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public ServicoSolicitacaoRequestDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getEndereco() {
        return endereco;
    }

    public ServicoSolicitacaoRequestDTO setEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public String getObservacao() {
        return observacao;
    }

    public ServicoSolicitacaoRequestDTO setObservacao(String observacao) {
        this.observacao = observacao;
        return this;
    }
}