package bomdestino.sgm.servicos.model.dto.response;

import bomdestino.sgm.servicos.model.dto.UsuarioDTO;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VagaSolicitacaoResponseDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("vaga")
    private VagaResponseDTO vaga;
    @JsonProperty("status")
    private StatusSolicitacaoEnum status;
    @JsonProperty("datahorasolicitacao")
    private LocalDateTime dataHoraSolicitacao;
    @JsonProperty("usuariosolicitacao")
    private UsuarioDTO usuarioSolicitacao;

    public Long getId() {
        return id;
    }

    public VagaSolicitacaoResponseDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public VagaResponseDTO getVaga() {
        return vaga;
    }

    public VagaSolicitacaoResponseDTO setVaga(VagaResponseDTO vaga) {
        this.vaga = vaga;
        return this;
    }

    public StatusSolicitacaoEnum getStatus() {
        return status;
    }

    public VagaSolicitacaoResponseDTO setStatus(StatusSolicitacaoEnum status) {
        this.status = status;
        return this;
    }

    public LocalDateTime getDataHoraSolicitacao() {
        return dataHoraSolicitacao;
    }

    public VagaSolicitacaoResponseDTO setDataHoraSolicitacao(LocalDateTime dataHoraSolicitacao) {
        this.dataHoraSolicitacao = dataHoraSolicitacao;
        return this;
    }

    public UsuarioDTO getUsuarioSolicitacao() {
        return usuarioSolicitacao;
    }

    public VagaSolicitacaoResponseDTO setUsuarioSolicitacao(UsuarioDTO usuarioSolicitacao) {
        this.usuarioSolicitacao = usuarioSolicitacao;
        return this;
    }
}