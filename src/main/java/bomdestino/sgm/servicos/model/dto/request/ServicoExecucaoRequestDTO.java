package bomdestino.sgm.servicos.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoExecucaoRequestDTO implements Serializable {

    @JsonProperty("idservicosolicitacao")
    private Long idServicoSolicitacao;
    @JsonProperty("responsavel_id")
    private Long responsavelId;
    @JsonProperty("responsavel_nome")
    private String responsavelNome;
    @JsonProperty("responsavel_telefone")
    private String responsavelTelefone;

    public Long getIdServicoSolicitacao() {
        return idServicoSolicitacao;
    }

    public ServicoExecucaoRequestDTO setIdServicoSolicitacao(Long idServicoSolicitacao) {
        this.idServicoSolicitacao = idServicoSolicitacao;
        return this;
    }

    public Long getResponsavelId() {
        return responsavelId;
    }

    public ServicoExecucaoRequestDTO setResponsavelId(Long responsavelId) {
        this.responsavelId = responsavelId;
        return this;
    }

    public String getResponsavelNome() {
        return responsavelNome;
    }

    public ServicoExecucaoRequestDTO setResponsavelNome(String responsavelNome) {
        this.responsavelNome = responsavelNome;
        return this;
    }

    public String getResponsavelTelefone() {
        return responsavelTelefone;
    }

    public ServicoExecucaoRequestDTO setResponsavelTelefone(String responsavelTelefone) {
        this.responsavelTelefone = responsavelTelefone;
        return this;
    }
}