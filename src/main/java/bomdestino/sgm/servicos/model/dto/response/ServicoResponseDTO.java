package bomdestino.sgm.servicos.model.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicoResponseDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("descricao")
    private String descricao;
    @JsonProperty("valor")
    private BigDecimal valor;
    @JsonProperty("prazo_atendimento")
    private Integer prazoAtendimento;
    @JsonProperty("unidade_medida_prazo")
    private String unidadeMedidaPrazo;

    public Long getId() {
        return id;
    }

    public ServicoResponseDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDescricao() {
        return descricao;
    }

    public ServicoResponseDTO setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public ServicoResponseDTO setValor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public Integer getPrazoAtendimento() {
        return prazoAtendimento;
    }

    public ServicoResponseDTO setPrazoAtendimento(Integer prazoAtendimento) {
        this.prazoAtendimento = prazoAtendimento;
        return this;
    }

    public String getUnidadeMedidaPrazo() {
        return unidadeMedidaPrazo;
    }

    public ServicoResponseDTO setUnidadeMedidaPrazo(String unidadeMedidaPrazo) {
        this.unidadeMedidaPrazo = unidadeMedidaPrazo;
        return this;
    }
}