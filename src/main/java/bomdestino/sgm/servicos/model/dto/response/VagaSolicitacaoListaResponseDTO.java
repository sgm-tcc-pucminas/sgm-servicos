package bomdestino.sgm.servicos.model.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VagaSolicitacaoListaResponseDTO implements Serializable {

    @JsonProperty("solicitacoes")
    private List<VagaSolicitacaoResponseDTO> solicitacoes;

    public VagaSolicitacaoListaResponseDTO() {
    }

    public VagaSolicitacaoListaResponseDTO(List<VagaSolicitacaoResponseDTO> solicitacoes) {
        this.solicitacoes = solicitacoes;
    }

    public List<VagaSolicitacaoResponseDTO> getSolicitacoes() {
        return solicitacoes;
    }

    public VagaSolicitacaoListaResponseDTO setSolicitacoes(List<VagaSolicitacaoResponseDTO> solicitacoes) {
        this.solicitacoes = solicitacoes;
        return this;
    }
}