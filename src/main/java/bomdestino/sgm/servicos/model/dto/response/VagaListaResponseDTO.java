package bomdestino.sgm.servicos.model.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VagaListaResponseDTO implements Serializable {

    @JsonProperty("vagas")
    private List<VagaResponseDTO> vagas;

    public VagaListaResponseDTO() {
    }

    public VagaListaResponseDTO(List<VagaResponseDTO> vagas) {
        this.vagas = vagas;
    }

    public List<VagaResponseDTO> getVagas() {
        return vagas;
    }

    public VagaListaResponseDTO setVagas(List<VagaResponseDTO> vagas) {
        this.vagas = vagas;
        return this;
    }
}