package bomdestino.sgm.servicos.model.dto.response;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;

import java.io.Serializable;
import java.util.List;

public class ServicoPublicoResponseDTO implements Serializable {

    private List<ServicoDTO> servicoLista;

    public ServicoPublicoResponseDTO() {
    }

    public ServicoPublicoResponseDTO(List<ServicoDTO> servicos) {
        this.servicoLista = servicos;
    }

    public List<ServicoDTO> getServicoLista() {
        return servicoLista;
    }

    public ServicoPublicoResponseDTO setServicoLista(List<ServicoDTO> servicoLista) {
        this.servicoLista = servicoLista;
        return this;
    }
}
