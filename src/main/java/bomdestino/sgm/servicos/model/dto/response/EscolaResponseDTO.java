package bomdestino.sgm.servicos.model.dto.response;

import bomdestino.sgm.servicos.model.dto.EscolaDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EscolaResponseDTO implements Serializable {

    @JsonProperty("escolas")
    private List<EscolaDTO> escolas;

    public EscolaResponseDTO() {
    }

    public EscolaResponseDTO(List<EscolaDTO> escolas) {
        this.escolas = escolas;
    }

    public List<EscolaDTO> getEscolas() {
        return escolas;
    }

    public EscolaResponseDTO setEscolas(List<EscolaDTO> escolas) {
        this.escolas = escolas;
        return this;
    }
}