package bomdestino.sgm.servicos.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity(name = "servico")
public class ServicoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String descricao;
    @Column
    private BigDecimal valor;
    @Column
    private Integer prazoAtendimento;
    @Column
    private String unidadeMedidaPrazo;

    public Long getId() {
        return id;
    }

    public ServicoEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDescricao() {
        return descricao;
    }

    public ServicoEntity setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public ServicoEntity setValor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public Integer getPrazoAtendimento() {
        return prazoAtendimento;
    }

    public ServicoEntity setPrazoAtendimento(Integer prazoAtendimento) {
        this.prazoAtendimento = prazoAtendimento;
        return this;
    }

    public String getUnidadeMedidaPrazo() {
        return unidadeMedidaPrazo;
    }

    public ServicoEntity setUnidadeMedidaPrazo(String unidadeMedidaPrazo) {
        this.unidadeMedidaPrazo = unidadeMedidaPrazo;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServicoEntity that = (ServicoEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}