package bomdestino.sgm.servicos.model.entity;

import bomdestino.sgm.servicos.util.converter.StatusSolicitacaoEnumConverter;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "servicosolicitacao")
public class ServicoSolicitacaoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name="idservico")
    private ServicoEntity servico;
    private Long idUsuarioSolicitacao;
    private Integer cep;
    private String endereco;
    private String observacao;
    @Column(name = "idstatussolicitacao")
    @Convert(converter = StatusSolicitacaoEnumConverter.class)
    private StatusSolicitacaoEnum status;
    private String responsavel;
    private String contato;
    private LocalDateTime dataHoraSolicitacao;

    public Long getId() {
        return id;
    }

    public ServicoSolicitacaoEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public ServicoEntity getServico() {
        return servico;
    }

    public ServicoSolicitacaoEntity setServico(ServicoEntity servico) {
        this.servico = servico;
        return this;
    }

    public Long getIdUsuarioSolicitacao() {
        return idUsuarioSolicitacao;
    }

    public ServicoSolicitacaoEntity setIdUsuarioSolicitacao(Long idUsuarioSolicitacao) {
        this.idUsuarioSolicitacao = idUsuarioSolicitacao;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public ServicoSolicitacaoEntity setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getEndereco() {
        return endereco;
    }

    public ServicoSolicitacaoEntity setEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public String getObservacao() {
        return observacao;
    }

    public ServicoSolicitacaoEntity setObservacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public StatusSolicitacaoEnum getStatus() {
        return status;
    }

    public ServicoSolicitacaoEntity setStatus(StatusSolicitacaoEnum status) {
        this.status = status;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public ServicoSolicitacaoEntity setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public ServicoSolicitacaoEntity setContato(String contato) {
        this.contato = contato;
        return this;
    }

    public LocalDateTime getDataHoraSolicitacao() {
        return dataHoraSolicitacao;
    }

    public ServicoSolicitacaoEntity setDataHoraSolicitacao(LocalDateTime dataHoraSolicitacao) {
        this.dataHoraSolicitacao = dataHoraSolicitacao;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServicoSolicitacaoEntity that = (ServicoSolicitacaoEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}