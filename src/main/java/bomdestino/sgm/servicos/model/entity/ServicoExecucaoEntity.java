package bomdestino.sgm.servicos.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "servicoexecucao")
public class ServicoExecucaoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Long idServicoSolicitacao;
    @Column
    private Long idResponsavel;
    @Column
    private Long idUsuarioAprovacao;
    @Column
    private Integer idStatusExecucao;
    @Column
    private LocalDateTime dataHoraExecucao;

    public Long getId() {
        return id;
    }

    public ServicoExecucaoEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getIdServicoSolicitacao() {
        return idServicoSolicitacao;
    }

    public ServicoExecucaoEntity setIdServicoSolicitacao(Long idServicoSolicitacao) {
        this.idServicoSolicitacao = idServicoSolicitacao;
        return this;
    }

    public Long getIdResponsavel() {
        return idResponsavel;
    }

    public ServicoExecucaoEntity setIdResponsavel(Long idResponsavel) {
        this.idResponsavel = idResponsavel;
        return this;
    }

    public Long getIdUsuarioAprovacao() {
        return idUsuarioAprovacao;
    }

    public ServicoExecucaoEntity setIdUsuarioAprovacao(Long idUsuarioAprovacao) {
        this.idUsuarioAprovacao = idUsuarioAprovacao;
        return this;
    }

    public Integer getIdStatusExecucao() {
        return idStatusExecucao;
    }

    public ServicoExecucaoEntity setIdStatusExecucao(Integer idStatusExecucao) {
        this.idStatusExecucao = idStatusExecucao;
        return this;
    }

    public LocalDateTime getDataHoraExecucao() {
        return dataHoraExecucao;
    }

    public ServicoExecucaoEntity setDataHoraExecucao(LocalDateTime dataHoraExecucao) {
        this.dataHoraExecucao = dataHoraExecucao;
        return this;
    }
}