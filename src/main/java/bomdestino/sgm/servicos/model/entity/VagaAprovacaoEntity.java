package bomdestino.sgm.servicos.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "vagaaprovacao")
public class VagaAprovacaoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "idvagaSolicitacao")
    private Long idVagaSolicitacao;
    @Column(name = "idusuarioaprovacao")
    private Long idUsuarioAprovacao;
    @Column(name = "datahoraaprovacao")
    private LocalDateTime dataHoraAprovacao;

    public Long getId() {
        return id;
    }

    public VagaAprovacaoEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getIdVagaSolicitacao() {
        return idVagaSolicitacao;
    }

    public VagaAprovacaoEntity setIdVagaSolicitacao(Long idVagaSolicitacao) {
        this.idVagaSolicitacao = idVagaSolicitacao;
        return this;
    }

    public Long getIdUsuarioAprovacao() {
        return idUsuarioAprovacao;
    }

    public VagaAprovacaoEntity setIdUsuarioAprovacao(Long idUsuarioAprovacao) {
        this.idUsuarioAprovacao = idUsuarioAprovacao;
        return this;
    }

    public LocalDateTime getDataHoraAprovacao() {
        return dataHoraAprovacao;
    }

    public VagaAprovacaoEntity setDataHoraAprovacao(LocalDateTime dataHoraAprovacao) {
        this.dataHoraAprovacao = dataHoraAprovacao;
        return this;
    }
}