package bomdestino.sgm.servicos.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "vagasolicitacao")
public class VagaSolicitacaoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name="idvaga")
    private VagaEntity vaga;
    private Long idUsuarioSolicitacao;
    private Integer idStatusSolicitacao;
    private LocalDateTime dataHoraSolicitacao;

    public Long getId() {
        return id;
    }

    public VagaSolicitacaoEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public VagaEntity getVaga() {
        return vaga;
    }

    public VagaSolicitacaoEntity setVaga(VagaEntity vaga) {
        this.vaga = vaga;
        return this;
    }

    public Long getIdUsuarioSolicitacao() {
        return idUsuarioSolicitacao;
    }

    public VagaSolicitacaoEntity setIdUsuarioSolicitacao(Long idUsuarioSolicitacao) {
        this.idUsuarioSolicitacao = idUsuarioSolicitacao;
        return this;
    }

    public Integer getIdStatusSolicitacao() {
        return idStatusSolicitacao;
    }

    public VagaSolicitacaoEntity setIdStatusSolicitacao(Integer idStatusSolicitacao) {
        this.idStatusSolicitacao = idStatusSolicitacao;
        return this;
    }

    public LocalDateTime getDatahorasolicitacao() {
        return dataHoraSolicitacao;
    }

    public VagaSolicitacaoEntity setDatahorasolicitacao(LocalDateTime datahorasolicitacao) {
        this.dataHoraSolicitacao = datahorasolicitacao;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VagaSolicitacaoEntity that = (VagaSolicitacaoEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}