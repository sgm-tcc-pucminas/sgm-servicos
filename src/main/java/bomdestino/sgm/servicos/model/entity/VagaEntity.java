package bomdestino.sgm.servicos.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "vaga")
public class VagaEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idEscola;
    private String nome;
    private String logradouro;
    private String bairro;
    private Integer cep;

    public Long getId() {
        return id;
    }

    public VagaEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getIdEscola() {
        return idEscola;
    }

    public VagaEntity setIdEscola(Long idEscola) {
        this.idEscola = idEscola;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public VagaEntity setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public VagaEntity setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public VagaEntity setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public VagaEntity setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VagaEntity that = (VagaEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}