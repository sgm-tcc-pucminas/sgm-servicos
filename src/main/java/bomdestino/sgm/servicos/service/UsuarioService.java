package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.model.dto.UsuarioDTO;
import bomdestino.sgm.servicos.service.component.RestSegurancaComponent;
import bomdestino.sgm.servicos.util.enums.ResourceSegurancaEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class UsuarioService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestSegurancaComponent restComponent;

    public UsuarioDTO obterPorId(Long idUsuario) {
        logger.info(format("Obtendo dados do usuario com id %d", idUsuario));
        return restComponent.get(format(ResourceSegurancaEnum.OBTER_USUARIO.getValue(), idUsuario), UsuarioDTO.class);
    }
}