package bomdestino.sgm.servicos.service.component;

import bomdestino.sgm.servicos.config.security.AuthenticationComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestSegurancaComponent {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AuthenticationComponent authentication;

    @Value("${seguranca.url}")
    private String url;

    public <T> T get(String resource, Class<T> responseType) {
        HttpHeaders headers = createHeaders();
        HttpEntity request = new HttpEntity(headers);
        String urlCompleta = getUrl(resource);
        return restTemplate.exchange(urlCompleta, HttpMethod.GET, request, responseType).getBody();
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(authentication.obterToken());
        return headers;
    }

    private String getUrl(String resource) {
        return url + resource;
    }
}
