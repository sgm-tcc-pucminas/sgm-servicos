package bomdestino.sgm.servicos.service.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestIntegracaoComponent {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${integracao.url}")
    private String url;

    public <T> T get(String resource, Class<T> responseType) {
        HttpHeaders headers = createHeaders();
        HttpEntity request = new HttpEntity(headers);
        String urlCompleta = getUrl(resource);
        return restTemplate.exchange(urlCompleta, HttpMethod.GET, request, responseType).getBody();
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    private String getUrl(String resource) {
        return url + resource;
    }
}
