package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoResponsavelReponseDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoResponseDTO;
import bomdestino.sgm.servicos.model.entity.ServicoEntity;
import bomdestino.sgm.servicos.repository.ServicoRepository;
import bomdestino.sgm.servicos.service.component.RestIntegracaoComponent;
import bomdestino.sgm.servicos.util.converter.ServicoConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static bomdestino.sgm.servicos.util.enums.ResourceIntegracaoEnum.SERVICO_RESPONSAVEL;
import static java.lang.String.format;

@Service
public class ServicoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServicoRepository repository;

    @Autowired
    private ServicoSolicitacaoService solicitacaoService;

    @Autowired
    private RestIntegracaoComponent restComponent;

    public ServicoResponseDTO criar(ServicoRequestDTO servico) {
        logger.info(format("Cadastrando o novo servico %s", 1));
        ServicoEntity entity = repository.save(new ServicoEntity()
                .setDescricao(servico.getDescricao())
                .setValor(servico.getValor())
                .setPrazoAtendimento(servico.getPrazoAtendimento())
                .setUnidadeMedidaPrazo(servico.getUnidadeMedidaPrazo()));

        return new ServicoResponseDTO()
                .setId(entity.getId())
                .setDescricao(entity.getDescricao())
                .setValor(entity.getValor())
                .setPrazoAtendimento(entity.getPrazoAtendimento())
                .setUnidadeMedidaPrazo(entity.getUnidadeMedidaPrazo());
    }

    public List<ServicoDTO> obterTodos() {
        logger.info("Obtendo todos os servicos cadastrados");
        List<ServicoEntity> servicos = repository.findAll();
        return ServicoConverter.convertList(servicos);
    }

    public ServicoResponsavelReponseDTO obterServicoResponsavel(Long idServico) {
        logger.info(format("Obter reponsavel pela execucao do servico id %s", idServico));
        return restComponent.get(obterResource(idServico), ServicoResponsavelReponseDTO.class);
    }

    private String obterResource(Long idServico) {
        return SERVICO_RESPONSAVEL.getValue() + idServico;
    }
}