package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.model.dto.EscolaDTO;
import bomdestino.sgm.servicos.model.dto.response.EscolaResponseDTO;
import bomdestino.sgm.servicos.service.component.RestIntegracaoComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static bomdestino.sgm.servicos.util.enums.ResourceIntegracaoEnum.ESCOLA_LISTA;
import static bomdestino.sgm.servicos.util.enums.ResourceIntegracaoEnum.ESCOLA_POR_ID;
import static java.lang.String.format;

@Service
public class EscolaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestIntegracaoComponent restComponent;

    public EscolaResponseDTO obter() {
        logger.info("Obtendo todas as escolas cadastradas");
        return restComponent.get(ESCOLA_LISTA.getValue(), EscolaResponseDTO.class);
    }

    public EscolaDTO obterPorId(Long idEscola) {
        logger.info(format("Obtendo dados da escola id %d", idEscola));
        return restComponent.get(format(ESCOLA_POR_ID.getValue(), idEscola), EscolaDTO.class);
    }
}