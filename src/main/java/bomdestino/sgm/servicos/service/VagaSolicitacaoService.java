package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.config.security.User;
import bomdestino.sgm.servicos.model.dto.EscolaDTO;
import bomdestino.sgm.servicos.model.dto.UsuarioDTO;
import bomdestino.sgm.servicos.model.dto.request.VagaSolicitacaoRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaResponseDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaSolicitacaoResponseDTO;
import bomdestino.sgm.servicos.model.entity.VagaAprovacaoEntity;
import bomdestino.sgm.servicos.model.entity.VagaEntity;
import bomdestino.sgm.servicos.model.entity.VagaSolicitacaoEntity;
import bomdestino.sgm.servicos.model.message.AprovacaoVagaMessage;
import bomdestino.sgm.servicos.producer.AprovacaoVagaProducer;
import bomdestino.sgm.servicos.repository.VagaAprovacaoRepository;
import bomdestino.sgm.servicos.repository.VagaSolicitacaoRepository;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static bomdestino.sgm.servicos.util.enums.PermissaoUsuarioEnum.ADMINISTRATIVE;
import static bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum.PENDENTE;
import static java.lang.String.format;

@Service
public class VagaSolicitacaoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VagaSolicitacaoRepository repository;

    @Autowired
    private VagaAprovacaoRepository vagaAprovacaoRepository;

    @Autowired
    private EscolaService escolaService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private AprovacaoVagaProducer producer;

    public VagaSolicitacaoResponseDTO criar(VagaSolicitacaoRequestDTO vaga, Long idUsuarioSolicitacao) {
        logger.info(format("Solicitando vaga id %d para o usuario id %d", vaga.getIdVaga(), idUsuarioSolicitacao));
        VagaSolicitacaoEntity entity = repository.save(new VagaSolicitacaoEntity()
                .setVaga(new VagaEntity().setId(vaga.getIdVaga()))
                .setIdUsuarioSolicitacao(idUsuarioSolicitacao)
                .setIdStatusSolicitacao(PENDENTE.getId())
                .setDatahorasolicitacao(LocalDateTime.now()));

        return new VagaSolicitacaoResponseDTO()
                .setId(entity.getId())
                .setVaga(new VagaResponseDTO()
                        .setId(entity.getVaga().getId())
                        .setNome(entity.getVaga().getNome())
                        .setLogradouro(entity.getVaga().getLogradouro())
                        .setBairro(entity.getVaga().getBairro())
                        .setCep(entity.getVaga().getCep())
                        .setIdEscola(entity.getVaga().getId()))
                .setDataHoraSolicitacao(entity.getDatahorasolicitacao())
                .setStatus(StatusSolicitacaoEnum.valueOf(entity.getIdStatusSolicitacao()));
    }

    public List<VagaSolicitacaoResponseDTO> obter(Integer idStatusSolicitacao) {
        StatusSolicitacaoEnum statusSolicitacao = StatusSolicitacaoEnum.valueOf(idStatusSolicitacao);
        logger.info(format("Obtendo solicitacoes com status %s", statusSolicitacao.getValue()));
        List<VagaSolicitacaoEntity> solicitacoes = repository.obterSolicitacaoPorStatus(statusSolicitacao);

        return solicitacoes.stream()
                .map(solicitacao -> {
                    EscolaDTO escola = escolaService.obterPorId(solicitacao.getVaga().getIdEscola());
                    UsuarioDTO usuario = usuarioService.obterPorId(solicitacao.getIdUsuarioSolicitacao());

                    return new VagaSolicitacaoResponseDTO()
                            .setId(solicitacao.getId())
                            .setVaga(new VagaResponseDTO()
                                    .setId(solicitacao.getVaga().getId())
                                    .setNome(solicitacao.getVaga().getNome())
                                    .setLogradouro(solicitacao.getVaga().getLogradouro())
                                    .setBairro(solicitacao.getVaga().getBairro())
                                    .setCep(solicitacao.getVaga().getCep())
                                    .setIdEscola(solicitacao.getVaga().getId())
                                    .setContato(escola.getContato())
                                    .setResponsavel(escola.getResponsavel()))
                            .setDataHoraSolicitacao(solicitacao.getDatahorasolicitacao())
                            .setStatus(StatusSolicitacaoEnum.valueOf(solicitacao.getIdStatusSolicitacao()))
                            .setUsuarioSolicitacao(usuario);
                })
                .collect(Collectors.toList());
    }

    public List<VagaSolicitacaoResponseDTO> obterPorUsuario(Long idUsuarioSolicitacao) {
        logger.info(format("Obtendo solicitacoes do usuario id %d", idUsuarioSolicitacao));
        List<VagaSolicitacaoEntity> solicitacoes = repository.obterPorUsuarioSolicitacao(idUsuarioSolicitacao);

        return solicitacoes.stream()
                .map(solicitacao -> {
                    EscolaDTO escola = escolaService.obterPorId(solicitacao.getVaga().getIdEscola());

                    return new VagaSolicitacaoResponseDTO()
                            .setId(solicitacao.getId())
                            .setVaga(new VagaResponseDTO()
                                    .setId(solicitacao.getVaga().getId())
                                    .setNome(solicitacao.getVaga().getNome())
                                    .setLogradouro(solicitacao.getVaga().getLogradouro())
                                    .setBairro(solicitacao.getVaga().getBairro())
                                    .setCep(solicitacao.getVaga().getCep())
                                    .setIdEscola(solicitacao.getVaga().getId())
                                    .setContato(escola.getContato())
                                    .setResponsavel(escola.getResponsavel()))
                            .setDataHoraSolicitacao(solicitacao.getDatahorasolicitacao())
                            .setStatus(StatusSolicitacaoEnum.valueOf(solicitacao.getIdStatusSolicitacao()));
                })
                .collect(Collectors.toList());
    }

    public VagaSolicitacaoResponseDTO obterPorId(Long idSolicitacao, User usuarioLogado) {
        logger.info(format("Obtendo solicitacao de id %d", idSolicitacao));
        Optional<VagaSolicitacaoEntity> solicitacaoOpt;

        if (usuarioLogado.getPermissions().contains(ADMINISTRATIVE.name())) {
            solicitacaoOpt = repository.findById(idSolicitacao);

        } else {
            solicitacaoOpt = repository.obterPorIdSolicitacaoUsuario(idSolicitacao, usuarioLogado.getId());
        }

        if (solicitacaoOpt.isPresent()) {
            VagaSolicitacaoEntity solicitacao = solicitacaoOpt.get();
            UsuarioDTO usuario = usuarioService.obterPorId(solicitacao.getIdUsuarioSolicitacao());

            return new VagaSolicitacaoResponseDTO()
                    .setId(solicitacao.getId())
                    .setVaga(new VagaResponseDTO()
                            .setId(solicitacao.getVaga().getId())
                            .setNome(solicitacao.getVaga().getNome())
                            .setLogradouro(solicitacao.getVaga().getLogradouro())
                            .setBairro(solicitacao.getVaga().getBairro())
                            .setCep(solicitacao.getVaga().getCep())
                            .setIdEscola(solicitacao.getVaga().getId()))
                    .setDataHoraSolicitacao(solicitacao.getDatahorasolicitacao())
                    .setStatus(StatusSolicitacaoEnum.valueOf(solicitacao.getIdStatusSolicitacao()))
                    .setUsuarioSolicitacao(usuario);

        } else {
            return null;
        }
    }

    @Transactional
    public void aprovarSolicitacao(Long idVagaSolicitacao, Long idUsuarioAprovacao) {
        logger.info(format("Aprovando a solicitacao de vaga id %d", idVagaSolicitacao));
        repository.aprovarSolicitacao(idVagaSolicitacao);
        vagaAprovacaoRepository.save(new VagaAprovacaoEntity()
                .setIdVagaSolicitacao(idVagaSolicitacao)
                .setIdUsuarioAprovacao(idUsuarioAprovacao)
                .setDataHoraAprovacao(LocalDateTime.now()));

        enviarAprovacaoVaga(idVagaSolicitacao, idUsuarioAprovacao);
    }

    private void enviarAprovacaoVaga(Long idVagaSolicitacao, Long idUsuarioAprovacao) {
        VagaSolicitacaoEntity solicitacao = repository.findById(idVagaSolicitacao).orElse(null);
        UsuarioDTO usuario = usuarioService.obterPorId(solicitacao.getIdUsuarioSolicitacao());

        producer.sendMessage(new AprovacaoVagaMessage()
                .setIdUsuarioAprovacao(idUsuarioAprovacao)
                .setDataHoraAprovacao(LocalDateTime.now())
                .setIdEscola(solicitacao.getVaga().getIdEscola())
                .setNome(usuario.getNome())
                .setTelefone(usuario.getTelefone())
                .setEmail(usuario.getEmail()));
    }
}