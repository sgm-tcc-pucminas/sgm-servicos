package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;
import bomdestino.sgm.servicos.model.dto.ServicoSolicitacaoDTO;
import bomdestino.sgm.servicos.model.dto.UsuarioDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoExecucaoRequestDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoSolicitacaoRequestDTO;
import bomdestino.sgm.servicos.model.entity.ServicoExecucaoEntity;
import bomdestino.sgm.servicos.model.entity.ServicoSolicitacaoEntity;
import bomdestino.sgm.servicos.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.servicos.model.message.ServicoStatusMessage;
import bomdestino.sgm.servicos.producer.ExecucaoServicoProducer;
import bomdestino.sgm.servicos.producer.ServicoStatusProducer;
import bomdestino.sgm.servicos.repository.ServicoExecucaoRepositoty;
import bomdestino.sgm.servicos.repository.ServicoSolicitacaoRepository;
import bomdestino.sgm.servicos.util.converter.SolicitacaoServicoConverter;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static bomdestino.sgm.servicos.util.enums.StatusExecucaoEnum.PROCESSANDO;
import static java.lang.String.format;

@Service
@EnableScheduling
public class ServicoSolicitacaoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServicoSolicitacaoRepository repository;

    @Autowired
    private ServicoExecucaoRepositoty servicoExecucaoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ExecucaoServicoProducer aprovacaoExecucaoServicoProducer;

    @Autowired
    private ServicoStatusProducer servicoStatusProducer;

    public List<ServicoSolicitacaoDTO> obterPorUsuario(Long idUsuarioLogado) {
        logger.info(format("Obtendo solicitacoes do usuario id %d", idUsuarioLogado));
        List<ServicoSolicitacaoEntity> solicitacoes = repository.findAllByIdUsuarioSolicitacaoOrderById(idUsuarioLogado);
        UsuarioDTO usuario = usuarioService.obterPorId(idUsuarioLogado);
        return SolicitacaoServicoConverter.convertList(solicitacoes, usuario);
    }

    public ServicoSolicitacaoDTO salvar(ServicoSolicitacaoRequestDTO solicitacao, Long idUsuarioLogado) {
        logger.info(format("Salvando nova solicitacao do usuario id %d", idUsuarioLogado));
        ServicoSolicitacaoEntity entity = repository.save(SolicitacaoServicoConverter.convert(solicitacao, idUsuarioLogado));
        UsuarioDTO usuario = usuarioService.obterPorId(idUsuarioLogado);
        return SolicitacaoServicoConverter.convert(entity, usuario);
    }

    public ServicoSolicitacaoDTO obterPorId(Long idSolicitacao, Long idUsuarioSolicitacao) {
        logger.info(format("Obtendo solicitacao id %d do usuario id %d", idSolicitacao, idUsuarioSolicitacao));
        Optional<ServicoSolicitacaoEntity> servicoSolicitacaoOpt = repository.findById(idSolicitacao);
        UsuarioDTO usuario = usuarioService.obterPorId(idUsuarioSolicitacao);

        if (servicoSolicitacaoOpt.isPresent()) {
            ServicoSolicitacaoEntity servicoSolicitacao = servicoSolicitacaoOpt.get();
            return new ServicoSolicitacaoDTO()
                    .setId(servicoSolicitacao.getId())
                    .setServico(new ServicoDTO()
                            .setId(servicoSolicitacao.getServico().getId())
                            .setDescricao(servicoSolicitacao.getServico().getDescricao()))
                    .setUsuarioSolicitacao(usuario)
                    .setCep(servicoSolicitacao.getCep())
                    .setEndereco(servicoSolicitacao.getEndereco())
                    .setObservacao(servicoSolicitacao.getObservacao())
                    .setDataHoraSolicitacao(servicoSolicitacao.getDataHoraSolicitacao())
                    .setStatus(servicoSolicitacao.getStatus().getValue());

        } else {
            return null;
        }
    }

    public List<ServicoSolicitacaoDTO> obterPorStatus(Integer idStatusSolicitacao) {
        StatusSolicitacaoEnum status = StatusSolicitacaoEnum.valueOf(idStatusSolicitacao);
        logger.info(format("Obtendo solicitacoes de servico com status %s", status.getValue()));
        List<ServicoSolicitacaoEntity> solicitacoes = repository.findAllByStatusOrderByServicoId(status);
        return solicitacoes.stream()
                .map(solicitacao ->
                        SolicitacaoServicoConverter.convert(solicitacao,
                                usuarioService.obterPorId(solicitacao.getIdUsuarioSolicitacao())))
                .collect(Collectors.toList());
    }

    public void aprovar(ServicoExecucaoRequestDTO servicoSolicitacao, Long idUsuarioAprovacao) {
        logger.info(format("Aprovar a solicitacao de servico id %d", servicoSolicitacao.getIdServicoSolicitacao()));
        atualizarSolicitacaoServico(servicoSolicitacao);
        servicoExecucaoRepository.save(new ServicoExecucaoEntity()
                .setIdServicoSolicitacao(servicoSolicitacao.getIdServicoSolicitacao())
                .setIdResponsavel(servicoSolicitacao.getResponsavelId())
                .setIdUsuarioAprovacao(idUsuarioAprovacao)
                .setIdStatusExecucao(PROCESSANDO.getId()));
    }

    public void atualizarStatusSolicitacao(Long idServicoSolicitacao, LocalDateTime dataHoraExecucao) {
        logger.info(format("Atualizando status da solicitacao id %d", idServicoSolicitacao));
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SGM-SERVICO ***] ---------------------------------------------------------");
        mensagem.append("\n\n [*** SGM-SERVICO ***] OBTENDO STATUS DA SOLICITACAO ID: " + idServicoSolicitacao);
        mensagem.append("\n\n [*** SGM-SERVICO ***] DATA HORA APROVACAO: " + dataHoraExecucao.toString());
        mensagem.append("\n\n [*** SGM-SERVICO ***] ---------------------------------------------------------\n\n");
        logger.info(mensagem.toString());

        atualizarSolicitacao(idServicoSolicitacao);
        atualizarServicoExecucao(idServicoSolicitacao, dataHoraExecucao);
    }

    @Scheduled(cron = "${agendador.atualizar-status-servico}")
    private void atualizarStatusSolicitacaoServico() {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SGM-SERVICO ***] ---------------------------------------------------------");
        mensagem.append("\n\n [*** SGM-SERVICO ***] EXECUTANDO SCHEDULER PARA ATUALIZAR STATUS DE SOLICITACAO");
        mensagem.append("\n\n [*** SGM-SERVICO ***] ---------------------------------------------------------\n\n");
        logger.info(mensagem.toString());

        List<ServicoSolicitacaoEntity> solicitacoes = repository.findAllByStatusOrderByServicoId(StatusSolicitacaoEnum.PROCESSANDO);
        solicitacoes.forEach(solicitacao -> servicoStatusProducer.sendMessage(new ServicoStatusMessage()
                .setIdSolicitacaoServico(solicitacao.getId())));
    }

    private void atualizarSolicitacaoServico(ServicoExecucaoRequestDTO servicoSolicitacao) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SGM-SERVICO ***] ---------------------------------------------------------");
        mensagem.append("\n\n [*** SGM-SERVICO ***] ATUALIZANDO STATUS DA SOLICITACAO ID " + servicoSolicitacao.getIdServicoSolicitacao());
        mensagem.append("\n\n [*** SGM-SERVICO ***] ---------------------------------------------------------\n\n");
        logger.info(mensagem.toString());

        ServicoSolicitacaoEntity solicitacao = repository
                .findById(servicoSolicitacao.getIdServicoSolicitacao()).orElse(null);

        repository.save(solicitacao
                .setResponsavel(servicoSolicitacao.getResponsavelNome())
                .setContato(servicoSolicitacao.getResponsavelTelefone())
                .setStatus(StatusSolicitacaoEnum.PROCESSANDO));

        aprovacaoExecucaoServicoProducer.sendMessage(new ExecucaoServicoMessage()
                .setIdServico(solicitacao.getServico().getId())
                .setNomeServico(solicitacao.getServico().getDescricao())
                .setValor(solicitacao.getServico().getValor())
                .setPrazoAtendimento(solicitacao.getServico().getPrazoAtendimento())
                .setUnidadeMedidaPrazo(solicitacao.getServico().getUnidadeMedidaPrazo())
                .setIdUsuarioSolicitacao(solicitacao.getIdUsuarioSolicitacao())
                .setCep(solicitacao.getCep())
                .setEndereco(solicitacao.getEndereco())
                .setObservacao(solicitacao.getObservacao())
                .setResponsavel(solicitacao.getResponsavel())
                .setContato(solicitacao.getContato())
                .setDataHoraSolicitacao(solicitacao.getDataHoraSolicitacao())
                .setDataHoraAprovacao(LocalDateTime.now()));
    }

    private void atualizarSolicitacao(Long idServicoSolicitacao) {
        ServicoSolicitacaoEntity solicitacao = repository.findById(idServicoSolicitacao).orElse(null);
        solicitacao.setStatus(StatusSolicitacaoEnum.FINALIZADA);
        repository.save(solicitacao);
    }

    private void atualizarServicoExecucao(Long idServicoSolicitacao, LocalDateTime dataHoraExecucao) {
        ServicoExecucaoEntity servicoExecucao = servicoExecucaoRepository.findAllByIdServicoSolicitacao(idServicoSolicitacao).orElse(null);
        servicoExecucao.setDataHoraExecucao(dataHoraExecucao);
        servicoExecucao.setIdStatusExecucao(StatusSolicitacaoEnum.FINALIZADA.getId());
        servicoExecucaoRepository.save(servicoExecucao);
    }
}