package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.model.dto.EscolaDTO;
import bomdestino.sgm.servicos.model.dto.request.VagaRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.VagaResponseDTO;
import bomdestino.sgm.servicos.model.entity.VagaEntity;
import bomdestino.sgm.servicos.repository.VagaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class VagaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VagaRepository repository;

    @Autowired
    private EscolaService escolaService;

    public VagaResponseDTO criar(VagaRequestDTO vaga) {
        logger.info(format("Criando vaga para a escola id %d", vaga.getIdEscola()));
        VagaEntity entity = repository.save(new VagaEntity()
                .setIdEscola(vaga.getIdEscola())
                .setNome(vaga.getNome())
                .setLogradouro(vaga.getLogradouro())
                .setBairro(vaga.getBairro())
                .setCep(vaga.getCep()));

        return new VagaResponseDTO()
                .setId(entity.getId())
                .setIdEscola(entity.getIdEscola())
                .setNome(entity.getNome())
                .setLogradouro(entity.getLogradouro())
                .setBairro(entity.getBairro())
                .setCep(entity.getCep());
    }

    public List<VagaResponseDTO> obterTodas() {
        logger.info("Obtendo todas as vagas disponiveis");
        List<VagaEntity> vagas = repository.obterVagasDisponiveis();

        if (vagas.isEmpty()) {
            return null;

        } else {
            return vagas.stream()
                    .map(vaga -> {
                        EscolaDTO escola = escolaService.obterPorId(vaga.getIdEscola());

                        return new VagaResponseDTO()
                                .setId(vaga.getId())
                                .setIdEscola(vaga.getIdEscola())
                                .setNome(vaga.getNome())
                                .setLogradouro(vaga.getLogradouro())
                                .setBairro(vaga.getBairro())
                                .setCep(vaga.getCep())
                                .setResponsavel(escola.getResponsavel())
                                .setContato(escola.getContato());
                    })
                    .collect(Collectors.toList());
        }
    }
}