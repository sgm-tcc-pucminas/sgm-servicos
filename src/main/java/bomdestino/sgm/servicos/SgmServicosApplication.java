package bomdestino.sgm.servicos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication
public class SgmServicosApplication {

	private static final Logger log = LoggerFactory.getLogger(SgmServicosApplication.class);

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(SgmServicosApplication.class, args);
		ConfigurableEnvironment environment = run.getEnvironment();

		log.info("\n\n\t------------------------------------------------------------\n\t" +
				":: SGM Servicos inicializado na porta: " + environment.getProperty("server.port") + " ::" +
				"\n\t------------------------------------------------------------\n\t");
	}
}
