package bomdestino.sgm.servicos.config.mensageria;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMQConfig {

    @Value("${spring.rabbitmq.host}")
    private String hostname;

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue servicoAtualizacaoStatusQueue() {
        return new Queue("servico-atualizacao-status", true, false, false);
    }

    @Bean
    FanoutExchange servicoAtualizacaoStatusExchange() {
        return new FanoutExchange("servico-atualizacao-status");
    }

    @Bean
    Binding servicoAtualizacaoStatusBinding(Queue servicoAtualizacaoStatusQueue,
                                            FanoutExchange servicoAtualizacaoStatusExchange) {

        return BindingBuilder.bind(servicoAtualizacaoStatusQueue).to(servicoAtualizacaoStatusExchange);
    }
}
