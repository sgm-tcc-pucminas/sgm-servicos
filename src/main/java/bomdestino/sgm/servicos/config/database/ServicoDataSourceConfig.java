package bomdestino.sgm.servicos.config.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "servicoEntityManagerFactory",
        transactionManagerRef = "servicoTransactionManager",
        basePackages = "bomdestino.sgm.servicos.repository"
)
@EnableTransactionManagement
public class ServicoDataSourceConfig {

    @Value("${spring.config.datasource.username}")
    private String username;

    @Value("${spring.config.datasource.password}")
    private String password;

    @Value("${spring.config.datasource.databases.servico.url}")
    private String url;

    @Value("${spring.config.datasource.databases.servico.driver-class-name}")
    private String driver;

    @Autowired
    private JpaVendorAdapter jpaVendorAdapter;

    @Bean(name = "servico-db")
    public DataSource servicoDataSource() {
        DriverManagerDataSource driverManager = new DriverManagerDataSource();
        driverManager.setDriverClassName(driver);
        driverManager.setUrl(url);
        driverManager.setUsername(username);
        driverManager.setPassword(password);
        return driverManager;
    }

    @Primary
    @Bean(name = "servicoEntityManagerFactory")
    public EntityManagerFactory servicoEntityManagerFactory(final @Qualifier("servico-db") DataSource servicoDataSource) {
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setDataSource(servicoDataSource);
        lef.setJpaVendorAdapter(jpaVendorAdapter);
        lef.setJpaProperties(JpaProperties.additional());
        lef.setPackagesToScan("bomdestino.sgm.servicos.model.entity");
        lef.setPersistenceUnitName("servicoDb");
        lef.afterPropertiesSet();
        return lef.getObject();
    }

    @Bean(name = "servicoTransactionManager")
    public PlatformTransactionManager servicoTransactionManager(@Qualifier("servicoEntityManagerFactory") EntityManagerFactory servicoEntityManagerFactory) {
        return new JpaTransactionManager(servicoEntityManagerFactory);
    }
}
