package bomdestino.sgm.servicos.repository;

import bomdestino.sgm.servicos.model.entity.VagaSolicitacaoEntity;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VagaSolicitacaoRepository extends JpaRepository<VagaSolicitacaoEntity, Long> {

    List<VagaSolicitacaoEntity> findAllByIdStatusSolicitacaoOrderByVagaId(Integer idStatusSolicitacao);

    List<VagaSolicitacaoEntity> findAllByIdUsuarioSolicitacaoOrderByVagaId(Long idUsuarioSolicitacao);

    Optional<VagaSolicitacaoEntity> findAllByIdAndIdUsuarioSolicitacaoOrderByVagaId(Long idSolicitacao, Long idUsuarioSolicitacao);

    @Modifying
    @Query(value = "UPDATE vagasolicitacao SET idstatussolicitacao = 3 WHERE id = :idVagaSolicitacao")
    void aprovarSolicitacao(@Param("idVagaSolicitacao") Long idVagaSolicitacao);

    default List<VagaSolicitacaoEntity> obterSolicitacaoPorStatus(StatusSolicitacaoEnum status) {
        return findAllByIdStatusSolicitacaoOrderByVagaId(status.getId());
    }

    default List<VagaSolicitacaoEntity> obterPorUsuarioSolicitacao(Long idUsuarioSolicitacao) {
        return findAllByIdUsuarioSolicitacaoOrderByVagaId(idUsuarioSolicitacao);
    }

    default Optional<VagaSolicitacaoEntity> obterPorIdSolicitacaoUsuario(Long idSolicitacao, Long idUsuarioSolicitacao) {
        return findAllByIdAndIdUsuarioSolicitacaoOrderByVagaId(idSolicitacao, idUsuarioSolicitacao);
    }
}