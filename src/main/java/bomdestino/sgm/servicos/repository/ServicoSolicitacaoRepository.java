package bomdestino.sgm.servicos.repository;

import bomdestino.sgm.servicos.model.entity.ServicoSolicitacaoEntity;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServicoSolicitacaoRepository extends JpaRepository<ServicoSolicitacaoEntity, Long> {

    List<ServicoSolicitacaoEntity> findAllByIdUsuarioSolicitacaoOrderById(Long idUsuarioSolicitacao);

    List<ServicoSolicitacaoEntity> findAllByStatusOrderByServicoId(StatusSolicitacaoEnum status);
}