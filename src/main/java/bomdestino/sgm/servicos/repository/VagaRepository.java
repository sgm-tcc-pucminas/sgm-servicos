package bomdestino.sgm.servicos.repository;

import bomdestino.sgm.servicos.model.entity.VagaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VagaRepository extends JpaRepository<VagaEntity, Long> {

    @Query(value = "SELECT v " +
            "\n FROM vaga v" +
            "\n LEFT JOIN vagasolicitacao vs ON vs.vaga.id = v.id" +
            "\n WHERE vs.id IS NULL" +
            "\n ORDER BY v.nome")
    List<VagaEntity> obterVagasDisponiveis();
}