package bomdestino.sgm.servicos.repository;

import bomdestino.sgm.servicos.model.entity.ServicoExecucaoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServicoExecucaoRepositoty extends JpaRepository<ServicoExecucaoEntity, Long> {

    Optional<ServicoExecucaoEntity> findAllByIdServicoSolicitacao(Long idServicoSolicitacao);
}