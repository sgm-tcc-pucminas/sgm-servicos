package bomdestino.sgm.servicos.repository;

import bomdestino.sgm.servicos.model.entity.VagaAprovacaoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VagaAprovacaoRepository extends JpaRepository<VagaAprovacaoEntity, Long> {
}