package bomdestino.sgm.servicos.util.token;

import bomdestino.sgm.servicos.config.security.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class TokenUtil {

    public User getAuthUser() {
        Authentication ret = SecurityContextHolder.getContext().getAuthentication();

        if (ret.getPrincipal() instanceof User) {
            return (User) ret.getPrincipal();
        }

        return null;
    }
}
