package bomdestino.sgm.servicos.util.enums;

public enum ResourceIntegracaoEnum {

    SERVICO_RESPONSAVEL("api/public/v1/servico/responsavel/"),
    ESCOLA_LISTA("/api/public/v1/educacao/escola"),
    ESCOLA_POR_ID("/api/public/v1/educacao/escola/%d"),
    ;

    private String value;

    ResourceIntegracaoEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
