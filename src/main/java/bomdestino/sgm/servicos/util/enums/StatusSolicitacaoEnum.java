package bomdestino.sgm.servicos.util.enums;

import static java.lang.String.format;

public enum StatusSolicitacaoEnum {

    PENDENTE(1, "Pendente"),
    PROCESSANDO(2, "Processando"),
    FINALIZADA(3, "Encerrada"),
    ;

    private Integer id;
    private String value;

    StatusSolicitacaoEnum(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public static StatusSolicitacaoEnum valueOf(Integer idStatus) {
        for (StatusSolicitacaoEnum status : StatusSolicitacaoEnum.values()) {
            if (status.getId().equals(idStatus)) {
                return status;
            }
        }

        throw new IllegalArgumentException(format("O idStatus %d informado é invalido", idStatus));
    }
}
