package bomdestino.sgm.servicos.util.enums;

public enum ResourceSegurancaEnum {

    OBTER_USUARIO("api/private/v1/usuario/%d"),
    ;

    private String value;

    ResourceSegurancaEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
