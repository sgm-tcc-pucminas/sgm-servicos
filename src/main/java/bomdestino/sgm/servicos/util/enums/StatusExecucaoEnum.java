package bomdestino.sgm.servicos.util.enums;

import static java.lang.String.format;

public enum StatusExecucaoEnum {

    PENDENTE(1, "Pendente"),
    PROCESSANDO(2, "Processando"),
    FINALIZADA(3, "Encerrada"),
    ;

    private Integer id;
    private String value;

    StatusExecucaoEnum(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public static StatusExecucaoEnum valueOf(Integer idStatus) {
        for (StatusExecucaoEnum status : StatusExecucaoEnum.values()) {
            if (status.getId().equals(idStatus)) {
                return status;
            }
        }

        throw new IllegalArgumentException(format("O idStatus %d informado é invalido", idStatus));
    }
}
