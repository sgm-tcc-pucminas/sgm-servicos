package bomdestino.sgm.servicos.util.converter;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;
import bomdestino.sgm.servicos.model.entity.ServicoEntity;

import java.util.List;
import java.util.stream.Collectors;

public class ServicoConverter {

    public static List<ServicoDTO> convertList(List<ServicoEntity> servicos) {
        return servicos.stream()
                .map(servico -> convert(servico))
                .collect(Collectors.toList());
    }

    public static ServicoDTO convert(ServicoEntity servico) {
        return new ServicoDTO()
                .setId(servico.getId())
                .setDescricao(servico.getDescricao())
                .setPrazo(servico.getPrazoAtendimento())
                .setUnidadePrazo(servico.getUnidadeMedidaPrazo())
                .setValor(servico.getValor());
    }
}
