package bomdestino.sgm.servicos.util.converter;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;
import bomdestino.sgm.servicos.model.dto.ServicoSolicitacaoDTO;
import bomdestino.sgm.servicos.model.dto.UsuarioDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoSolicitacaoRequestDTO;
import bomdestino.sgm.servicos.model.entity.ServicoEntity;
import bomdestino.sgm.servicos.model.entity.ServicoSolicitacaoEntity;
import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class SolicitacaoServicoConverter {

    public static List<ServicoSolicitacaoDTO> convertList(List<ServicoSolicitacaoEntity> servicos, UsuarioDTO usuario) {
        return servicos.stream()
                .map(solicitacao -> convert(solicitacao, usuario))
                .collect(Collectors.toList());
    }

    public static ServicoSolicitacaoDTO convert(ServicoSolicitacaoEntity solicitacao, UsuarioDTO usuario) {
        return new ServicoSolicitacaoDTO()
                .setId(solicitacao.getId())
                .setServico(new ServicoDTO()
                        .setId(solicitacao.getServico().getId())
                        .setDescricao(solicitacao.getServico().getDescricao()))
                .setUsuarioSolicitacao(usuario)
                .setCep(solicitacao.getCep())
                .setEndereco(solicitacao.getEndereco())
                .setObservacao(solicitacao.getObservacao())
                .setStatus(solicitacao.getStatus().getValue())
                .setDataHoraSolicitacao(solicitacao.getDataHoraSolicitacao())
                .setResponsavel(solicitacao.getResponsavel())
                .setContato(solicitacao.getContato())
                .setStatus(solicitacao.getStatus().getValue());
    }

    public static ServicoSolicitacaoEntity convert(ServicoSolicitacaoRequestDTO solicitacao, Long idUsuarioLogado) {
        return new ServicoSolicitacaoEntity()
                .setServico(new ServicoEntity().setId(solicitacao.getIdServico()))
                .setIdUsuarioSolicitacao(idUsuarioLogado)
                .setCep(solicitacao.getCep())
                .setEndereco(solicitacao.getEndereco())
                .setObservacao(solicitacao.getObservacao())
                .setStatus(StatusSolicitacaoEnum.PENDENTE)
                .setDataHoraSolicitacao(LocalDateTime.now());
    }
}
