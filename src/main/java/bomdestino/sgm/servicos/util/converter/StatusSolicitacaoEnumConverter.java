package bomdestino.sgm.servicos.util.converter;

import bomdestino.sgm.servicos.util.enums.StatusSolicitacaoEnum;

import javax.persistence.AttributeConverter;

public class StatusSolicitacaoEnumConverter implements AttributeConverter<StatusSolicitacaoEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(StatusSolicitacaoEnum statusSolicitacaoEnum) {
        return statusSolicitacaoEnum.getId();
    }

    @Override
    public StatusSolicitacaoEnum convertToEntityAttribute(Integer idStatusSolicitacao) {
        return StatusSolicitacaoEnum.valueOf(idStatusSolicitacao);
    }
}
