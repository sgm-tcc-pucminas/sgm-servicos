package bomdestino.sgm.servicos.service;

import bomdestino.sgm.servicos.model.dto.ServicoDTO;
import bomdestino.sgm.servicos.model.dto.ServicoResponsavelDTO;
import bomdestino.sgm.servicos.model.dto.request.ServicoRequestDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoResponsavelReponseDTO;
import bomdestino.sgm.servicos.model.dto.response.ServicoResponseDTO;
import bomdestino.sgm.servicos.model.entity.ServicoEntity;
import bomdestino.sgm.servicos.repository.ServicoRepository;
import bomdestino.sgm.servicos.service.component.RestIntegracaoComponent;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@DisplayName("Testes da Classe ServicoService")
public class ServicoServiceTest {

    @InjectMocks
    private ServicoService service;
    @Mock
    private ServicoRepository repository;
    @Mock
    private ServicoSolicitacaoService solicitacaoService;
    @Mock
    private RestIntegracaoComponent restComponent;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void criarTest() {
        ServicoRequestDTO request = new ServicoRequestDTO()
                .setDescricao("Servico Teste")
                .setValor(BigDecimal.ZERO)
                .setPrazoAtendimento(1)
                .setUnidadeMedidaPrazo("dia");

        ServicoEntity entity = new ServicoEntity()
                .setId(1L)
                .setDescricao(request.getDescricao())
                .setValor(request.getValor())
                .setPrazoAtendimento(request.getPrazoAtendimento())
                .setUnidadeMedidaPrazo(request.getUnidadeMedidaPrazo());

        when(repository.save(any())).thenReturn(entity);

        ServicoResponseDTO response = service.criar(request);
        assertNotNull(response.getId());
        assertEquals(request.getDescricao(), response.getDescricao());
        assertEquals(request.getValor(), response.getValor());
        assertEquals(request.getPrazoAtendimento(), response.getPrazoAtendimento());
        assertEquals(request.getUnidadeMedidaPrazo(), response.getUnidadeMedidaPrazo());
    }

    @Test
    public void obterTodosTest() {
        ServicoEntity entity = new ServicoEntity()
                .setId(1L)
                .setDescricao("Servico Teste")
                .setValor(BigDecimal.ZERO)
                .setPrazoAtendimento(1)
                .setUnidadeMedidaPrazo("dia");

        when(repository.findAll()).thenReturn(asList(entity));

        List<ServicoDTO> resultList = service.obterTodos();
        assertEquals(1, resultList.size());

        ServicoDTO result = resultList.get(0);
        assertEquals(entity.getId(), result.getId());
        assertEquals(entity.getDescricao(), result.getDescricao());
        assertEquals(entity.getValor(), result.getValor());
        assertEquals(entity.getPrazoAtendimento(), result.getPrazo());
        assertEquals(entity.getUnidadeMedidaPrazo(), result.getUnidadePrazo());
    }

    @Test
    public void obterServicoResponsavelTest() {
        Long idServico = 123L;

        ServicoResponsavelDTO responsavel = new ServicoResponsavelDTO()
                .setId(1L)
                .setNome("Solzanir")
                .setDocumentoIdentificacao("987.654.321-10")
                .setTelefone("(19) 98765-4321")
                .setEmail("teste@bomdestino.com.br");

        when(restComponent.get(any(), any())).thenReturn(new ServicoResponsavelReponseDTO()
                .setResponsaveis(asList(responsavel)));

        ServicoResponsavelReponseDTO response = service.obterServicoResponsavel(idServico);
        assertEquals(1, response.getResponsaveis());

        ServicoResponsavelDTO result = response.getResponsaveis().get(0);
        assertEquals(responsavel.getId(), result.getId());
        assertEquals(responsavel.getNome(), result.getNome());
        assertEquals(responsavel.getDocumentoIdentificacao(), result.getDocumentoIdentificacao());
        assertEquals(responsavel.getTelefone(), result.getTelefone());
        assertEquals(responsavel.getEmail(), result.getEmail());
    }
}